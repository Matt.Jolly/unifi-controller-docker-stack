# Unifi Controller Docker Stack

This is a docker stack file for Unifi Controller. It is designed to be run behind Traefik. If you are not deploying in stack mode, please ensure that you amend the labels (thanks, compose-file "specification".

## Usage

Amend your traefik configuration so that it will connect to self-signed certificates (note: This is a global setting)

```toml
# Unifi Controller has its own self-signed certificate behind traefik
[serversTransport]
  insecureSkipVerify = true
```

Configure `docker-compose.yml` to suit your environment; this probably means setting the URI.

Start and configurate your Unifi controller via the WebUI. Set the `inform host` to an appropriate value for your network; a DNS host is usually best.

